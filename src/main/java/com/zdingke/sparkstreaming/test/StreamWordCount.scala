package com.zdingke.sparkstreaming.test

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by Administrator on 2016/12/20.
  */
object StreamWordCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("streamcount")
    val ssc = new StreamingContext(conf,Seconds(20))
    val socketStream1 = ssc.socketTextStream("10.104.205.96",11111)
    val words = socketStream1.flatMap(_.split(" "))
    val pairs = words.map(word=>(word,1))
    pairs.reduceByKey(_+_).print()
    ssc.start()
    ssc.awaitTermination()
  }
}

package com.zdingke.scala.test

import java.util

/**
  * Created by Administrator on 2016/12/2.
  */
object HelloWorld {
  def main(args: Array[String]): Unit = {
//    println(test("woai"))
    caseTest();
  }

  def test(n:String) = {
    val t = "dingke"+n
    t
  }

  def caseTest(): Unit ={
    val score = Map((1,"dingke"),(2,"dingke"))
    val score1 = Seq((1,"dingke"),(2,"dingke"))
    score.foreach(e=>println(e._1+"."+e.swap))
    score1.foreach{e=>println(e._1+""+e._2)}
    score1.foreach{case (v,k)=>println(v+""+k)}
    score1.foreach{case(k,v)=>println(k+","+v)}
    score1.foreach(e=>println(e._1+"."+e._2))
  }

  def maptest(): Unit ={
    val score1 = Seq("1","2")
//    score1.toList.map({case k=>k->_})
  }

  def test: Unit ={
    val data = Array("362719342,10.4.6.24,4:04,0028,20160203T152325;03,0026,20160203T162324")
    data.map(e=>{
      val s = e.split(":")
      (s(0),s(1))
    })

    data.map(_.split(":")).map(e=>(e(0),e(1)))
  }
}

package com.zdingke.sparkcore.test

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Administrator on 2016/12/20.
  */
object SecondSort {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("secondsort")
    val sc = new SparkContext(conf)
    val line = sc.parallelize(Array(("c",1),("c",5),("c",3),("a",1),("a",2),("a",3),("b",2),("b",5)),1)
    line.groupByKey().sortByKey(true).map(l=>(l._1,l._2.toList.sortWith(_.toInt<_.toInt)))
      .collect().foreach(println)
  }
}

package com.zdingke.sparkcore.test

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Administrator on 2016/12/21.
  */
object WordCountNext {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("secondsort")
    val sc = new SparkContext(conf)

    val data = sc.parallelize(Array("A;B;C;D;B;C;D","A;C;D;B;C;D"))
    data.map(_.split(";")).flatMap(x=>{
      for(i<-0 until x.length-1) yield (x(i)+","+x(i+1),1)
    }).reduceByKey(_+_).foreach(println)
  }
}

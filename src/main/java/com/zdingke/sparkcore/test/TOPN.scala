package com.zdingke.sparkcore.test

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Administrator on 2016/12/19.
  */
object TOPN {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("topn");
    val sc = new SparkContext(conf);
    val line = sc.parallelize(Array(("a",1),("a",2),("b",1),("b",2),("c",5),("c",1)))
    val groupsort = line.groupByKey().map(tu=>{
      val key=tu._1
      val values=tu._2
      val sortValues=values.toList.sortWith(_>_).take(2)
      (key,sortValues)
    })
    val top = groupsort.flatMap({case (key, numbers) => numbers.map(key -> _)})
  }
}

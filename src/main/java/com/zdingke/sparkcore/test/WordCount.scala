package com.zdingke.sparkcore.test

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Administrator on 2016/12/2.
  */
object WordCount {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    val sc = new SparkContext(conf)
//    val line = sc.textFile(args(0))
    sc.parallelize(1 until 1000000).map( x => (x%30,x)).groupByKey().count
    sc.stop()
  }
}

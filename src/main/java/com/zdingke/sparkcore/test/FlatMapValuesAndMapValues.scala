package com.zdingke.sparkcore.test

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Administrator on 2016/12/21.
  */
object FlatMapValuesAndMapValues {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("secondsort")
    val sc = new SparkContext(conf)

    val rrd = sc.makeRDD(Array((1,"A"),(2,"B"),(3,"C"),(4,"D")),2)
    rrd.flatMapValues(x=>x+"_").collect().foreach(println)
    rrd.mapValues(x=>x+"_").collect().foreach(println)
  }
}

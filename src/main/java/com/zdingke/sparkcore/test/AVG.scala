package com.zdingke.sparkcore.test

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Administrator on 2016/12/19.
  */
object AVG {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("avg");
    val sc = new SparkContext(conf);
    val line = sc.parallelize(Array(("a",1),("a",2),("c",1)))
    line.map(l=>(l._1,(1,l._2))).reduceByKey((v1,v2)=>(v1._1+v2._1,v1._2+v2._2))
      .map(l=>(l._1,l._2._2/l._2._1))
      .collect()
      .foreach(println)
  }
}
